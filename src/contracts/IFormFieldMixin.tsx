export default interface IFormFieldMixin {
  value?: string | boolean;
  onInput?(value: unknown): void;
  valid?: boolean | null;
}
