import { useRecoilState } from 'recoil';
import * as React from 'react';
import ActiveTableComponent from '../components/activeTable/ActiveTableComponent';
import tablesState from '../atoms/tablesState';
import GridComponent from '../components/grid/GridComponent';
import ActionsListComponent from '../components/actionsList/ActionsListComponent';
import ColumnComponent from '../components/grid/ColumnComponent';
import VerticalFormContainer from '../containers/VerticalFormContainer';
import HorizontalFormContainer from '../containers/HorizontalFormContainer';
import createState, { getInitialCreateState, ICreateState } from '../atoms/createState';
import DialogContainer from '../containers/DialogContainer';
import editModalVisibilityState from '../atoms/editModalVisibilityState';
import editState, { getInitialEditState, IEditState } from '../atoms/editState';
import { ITableRecord } from '../components/table/TableComponent';

const Home: React.FC = () => {
  const [tables, setTables] = useRecoilState(tablesState);
  const [create, setCreate] = useRecoilState(createState);
  const [editModalVisibility, setEditModalVisibility] = useRecoilState(editModalVisibilityState);
  const [edit, setEdit] = useRecoilState(editState);

  const getCreateSubmitHandler = (tableKey: string) => (value: ICreateState) => {
    setTables(tables.map((table) => (
      table.key === tableKey ? {
        ...table,
        data: [...(table.data || []), value],
      } : table
    )));
    setCreate(getInitialCreateState());
  };

  const copyTable = () => {
    setTables([
      ...tables,
      {
        ...JSON.parse(JSON.stringify(tables[0])),
        key: `${tables[0].key}${tables.length}`,
      },
    ]);
  };

  const getTableDeleter = (tableKey: string) => () => {
    setTables(tables.filter((table) => table.key !== tableKey));
  };

  const getRowDeleter = (tableKey: string, index: number) => () => {
    setTables(tables.map((table) => (
      tableKey === table.key ? {
        ...table,
        data: (table.data || []).filter((row, rowIndex) => rowIndex !== index),
      } : table
    )));
  };

  const getRowEditor = (tableKey: string, index: number) => () => {
    setEdit({
      ...getInitialEditState(),
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      ...tables.find((table) => table.key === tableKey)?.data[index],
      index,
      tableKey,
    });
    setEditModalVisibility(true);
  };

  const closeEditModal = () => {
    setEditModalVisibility(false);
  };

  const editSubmitHandler = (value: IEditState) => {
    setTables(tables.map((table) => (
      table.key === value.tableKey ? {
        ...table,
        data: (table.data || []).map((record, index) => (
          index === value.index ? {
            ...(JSON.parse(JSON.stringify(record))),
            name: value.name,
            surname: value.surname,
            city: value.city,
          } as ITableRecord : record
        )),
      } : table
    )));
  };

  return (
    <div>
      {editModalVisibility && (
        <DialogContainer
          value={edit}
          onInput={setEdit}
          onSubmit={editSubmitHandler}
          onClose={closeEditModal}
        />
      )}
      <GridComponent wrap>
        {tables.map((table, index) => (
          <ColumnComponent width="100%" key={table.key}>
            <GridComponent wrap>
              <ColumnComponent width="100%">
                <ActiveTableComponent
                  key={table.key}
                  table={{
                    keyField: 'name',
                    data: table.data,
                    columns: [
                      {
                        field: 'name',
                        label: 'Name',
                        // width: 95,
                      },
                      {
                        field: 'surname',
                        label: 'Surname',
                        // width: 130,
                      },
                      {
                        field: 'age',
                        label: 'Age',
                        // width: 98,
                      },
                      {
                        field: 'city',
                        label: 'City',
                        // width: 80,
                      },
                      {
                        width: 200,
                        field: 'actions',
                        render: (row, rowIndex) => (
                          <ActionsListComponent
                            grow
                            actions={[
                              {
                                key: 'edit',
                                label: 'Edit',
                                state: 'primary',
                                width: 56,
                                onClick: getRowEditor(table.key, rowIndex),
                              },
                              {
                                key: 'delete',
                                label: 'Delete',
                                state: 'danger',
                                width: 46,
                                onClick: getRowDeleter(table.key, rowIndex),
                              },
                            ]}
                          />
                        ),
                      },
                    ],
                  }}
                  actions={{
                    actions: index === 0 ? [
                      {
                        key: 'copy',
                        label: 'Copy table',
                        state: 'primary-filled',
                        onClick: copyTable,
                      },
                    ] : [
                      {
                        key: 'delete',
                        state: 'icon',
                        icon: 'close',
                        width: 50,
                        onClick: getTableDeleter(table.key),
                      },
                    ],
                  }}
                />
              </ColumnComponent>
              {index === 0 && (
                <>
                  <ColumnComponent width="100%">
                    <VerticalFormContainer
                      value={create}
                      onInput={setCreate}
                      onSubmit={getCreateSubmitHandler(table.key)}
                    />
                  </ColumnComponent>
                  <ColumnComponent width="100%">
                    <HorizontalFormContainer
                      value={create}
                      onInput={setCreate}
                      onSubmit={getCreateSubmitHandler(table.key)}
                    />
                  </ColumnComponent>
                </>
              )}
            </GridComponent>
          </ColumnComponent>
        ))}
      </GridComponent>
    </div>
  );
};

export default Home;
