import * as React from 'react';
import './column.scss';
import classNames from '../../utils/classNames';
import pxOrValue from '../../utils/pxOrValue';

interface IColumnComponent {
  align?: 'center' | 'start' | 'end';
  justify?: 'center' | 'start' | 'end';
  width?: number | string;
}

const ColumnComponent: React.FC<IColumnComponent> = ({
  children,
  justify,
  align,
  width,
}) => (
  <div
    className={classNames([
      'column',
      justify && `column--justify-${justify}`,
      align && `column--align-${align}`,
    ])}
    style={{ width: pxOrValue(width) }}
  >
    {children}
  </div>
);

export default ColumnComponent;
