import * as React from 'react';
import './grid.scss';
import classNames from '../../utils/classNames';

interface IGridComponent {
  align?: 'center' | 'start' | 'end';
  justify?: 'center' | 'start' | 'end';
  direction?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
  wrap?: boolean;
}

const GridComponent: React.FC<IGridComponent> = ({
  children,
  justify = 'start',
  align = 'normal',
  direction = 'row',
  wrap = false,
}) => (
  <div className={classNames([
    'grid',
    `grid--justify-${justify}`,
    `grid--align-${align}`,
    `grid--direction-${direction}`,
    wrap && 'grid--wrap',
  ])}
  >
    {children}
  </div>
);

export default GridComponent;
