import * as React from 'react';
import TableComponent, { ITableComponent } from '../table/TableComponent';
import ActionsListComponent, { IActionsListComponent } from '../actionsList/ActionsListComponent';
import './activeTable.scss';

interface IActiveTableComponent {
  table: ITableComponent;
  actions?: IActionsListComponent;
}

const ActiveTableComponent: React.FC<IActiveTableComponent> = ({
  table,
  actions,
}) => (
  <div className="active-table">
    {actions && (
      <div className="active-table__actions">
        <ActionsListComponent {...actions} />
      </div>
    )}
    <div className="action-table__table">
      <TableComponent {...table} />
    </div>
  </div>
);

export default ActiveTableComponent;
