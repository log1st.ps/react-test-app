import * as React from 'react';
import './textInput.scss';
import IFormFieldMixin from '../../contracts/IFormFieldMixin';
import classNames from '../../utils/classNames';

interface ITextInputComponent extends IFormFieldMixin {
  placeholder?: string;
}

const TextInputComponent: React.FC<ITextInputComponent> = ({
  placeholder,
  value,
  onInput,
  valid = null,
}) => {
  const handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    onInput && onInput(event.target.value || '');
  };

  return (
    <input
      className={classNames([
        'text-input',
        typeof valid === 'boolean' && `text-input--${valid ? 'valid' : 'invalid'}`,
      ])}
      placeholder={placeholder}
      onInput={handleInput}
      value={String(value)}
    />
  );
};

export default TextInputComponent;
