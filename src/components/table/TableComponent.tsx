import * as React from 'react';
import './table.scss';
import classNames from '../../utils/classNames';
import pxOrValue from '../../utils/pxOrValue';
import { ICreateState } from '../../atoms/createState';

export type ITableRecord = ICreateState;

type ITableColumn = {
  field: string;
  label?: string;
  render?(record: ITableRecord, index: number): string | number | JSX.Element;
  width?: number;
};

export interface ITableComponent {
  columns?: ITableColumn[];
  data?: ITableRecord[];
  keyField: string;
}

const TableComponent: React.FC<ITableComponent> = ({
  columns = [],
  data = [],
  keyField,
}) => (
  <div className="table">
    <table className="table__table">
      <thead className="table__head">
        <tr className="table__head-row">
          {columns.map((column) => (
            <th
              key={column.field}
              className={classNames(['table__cell', 'table__cell--head'])}
              style={{ width: pxOrValue(column.width) }}
            >
              {column.label && column.label}
            </th>
          ))}
        </tr>
      </thead>
      <tbody className="table__body">
        {data.map((record, index) => (
          <tr key={String(record[keyField as keyof ITableRecord])} className="table__body-row">
            {columns.map((column) => (
              <td key={column.field} className={classNames(['table__cell', 'table__cell--body'])}>
                <div className="table__cell-content">
                  {
                        column.render
                          ? column.render(record, index)
                          : record[column.field as keyof ITableRecord]
                    }
                </div>
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);

export default TableComponent;
