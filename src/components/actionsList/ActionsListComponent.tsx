import * as React from 'react';
import classNames from '../../utils/classNames';
import IconComponent, { IIconComponent } from '../icon/IconComponent';
import './actionsList.scss';
import pxOrValue from '../../utils/pxOrValue';

type IAction = {
  key: string;
  label?: string;
  state?: 'danger' | 'primary' | 'primary-filled' | 'icon';
  size?: 'sm' | 'xl';
  icon?: IIconComponent['type'];
  width?: number;
  onClick?: () => void;
};

export interface IActionsListComponent {
  actions: IAction[];
  grow?: boolean;
}

const ActionsListComponent: React.FC<IActionsListComponent> = ({
  actions,
  grow,
}) => (
  <div className={classNames(['actions', grow && 'actions--grow'])}>
    {actions.map((action) => (
      <button
        type="button"
        key={action.key}
        className={classNames([
          'actions__action',
          action.state && `actions__action--${action.state}`,
          `actions__action--${action.size || 'sm'}`,
        ])}
        style={{ width: pxOrValue(action.width) }}
        onClick={action.onClick}
      >
        {action.label && action.label}
        {action.icon && (
          <IconComponent size={14} type={action.icon} />
        )}
      </button>
    ))}
  </div>
);

export default ActionsListComponent;
