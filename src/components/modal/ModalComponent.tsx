import * as React from 'react';
import PanelComponent from '../panel/PanelComponent';
import IconComponent from '../icon/IconComponent';
import './modal.scss';
import classNames from '../../utils/classNames';

export interface IModalComponent {
  title?: string;
  value?: boolean;
  onInput?(value: boolean): void;
}

const ModalComponent: React.FC<IModalComponent> = ({
  children,
  title,
  value: isActive = false,
  onInput: setIsActive,
}) => {
  const onClose = () => {
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    setIsActive && setIsActive(false);
  };

  const onCloseKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'Enter') {
      onClose();
    }
  };

  return (
    <div className={classNames([
      'modal',
      isActive && 'modal--active',
    ])}
    >
      <div className="modal__dialog">
        <PanelComponent>
          <div className="modal__header">
            <div className="modal__title">{title}</div>
            <div
              onKeyDown={onCloseKeyDown}
              tabIndex={0}
              role="button"
              className="modal__close"
              onClick={onClose}
            >
              <IconComponent type="close" size={14} />
            </div>
          </div>
          <div className="modal__body">
            {children}
          </div>
        </PanelComponent>
      </div>
    </div>
  );
};

export default ModalComponent;
