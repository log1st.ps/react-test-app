import * as React from 'react';
import './checkbox.scss';
import IconComponent from '../icon/IconComponent';
import IFormFieldMixin from '../../contracts/IFormFieldMixin';
import classNames from '../../utils/classNames';

interface ICheckboxComponent extends IFormFieldMixin {
  label?: string;
}

const CheckboxComponent: React.FC<ICheckboxComponent> = ({
  label,
  value,
  onInput,
  valid = null,
}) => {
  const handleInput = () => {
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    onInput && onInput(!value);
  };
  const onClick = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'Enter') {
      handleInput();
    }
  };

  return (
    <div
      className={classNames([
        'checkbox',
        typeof valid === 'boolean' && `checkbox--${valid ? 'valid' : 'invalid'}`,
      ])}
      onKeyDown={onClick}
      role="button"
      tabIndex={0}
      onClick={handleInput}
    >
      <div className="checkbox__check">
        {value && (
          <IconComponent type="check" size={14} />
        )}
      </div>
      {label && (
        <div className="checkbox__label">{label}</div>
      )}
    </div>
  );
};

export default CheckboxComponent;
