/* eslint import/no-dynamic-require: 0 */
/* eslint global-require: 0 */
/* eslint @typescript-eslint/no-unsafe-assignment: 0 */
/* eslint @typescript-eslint/no-var-requires: 0 */
/* eslint @typescript-eslint/no-unsafe-member-access: 0 */

import * as React from 'react';
import classNames from '../../utils/classNames';
import { ReactComponent as IconClose } from './assets/close.svg';
import { ReactComponent as IconCheck } from './assets/check.svg';
import './icon.scss';
import pxOrValue from '../../utils/pxOrValue';

export interface IIconComponent {
  type: 'close' | 'check';
  size?: number;
}

const iconsMap = {
  close: IconClose,
  check: IconCheck,
};

const IconComponent: React.FC<IIconComponent> = ({
  type,
  size = 0,
}) => {
  const Component = iconsMap[type];
  return (
    <Component
      style={{ width: pxOrValue(size || 0), height: pxOrValue(size || 0) }}
      className={classNames(['icon', `icon--${type}`])}
    />
  );
};

export default IconComponent;
