import * as React from 'react';
import './error.scss';

interface IErrorComponent {
  text?: string;
}

const ErrorComponent: React.FC<IErrorComponent> = ({
  text,
}) => (
  <div className="error">
    {text}
  </div>
);

export default ErrorComponent;
