import * as React from 'react';
import './panel.scss';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IPanelComponent {

}

const PanelComponent: React.FC<IPanelComponent> = ({ children }) => (
  <div className="panel">
    {children}
  </div>
);

export default PanelComponent;
