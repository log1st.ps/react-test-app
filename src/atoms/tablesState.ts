import { atom } from 'recoil';
import { ITableComponent } from '../components/table/TableComponent';

const getInitialTablesState: () => (Array<{
  key: string,
  data: ITableComponent['data']
}>) = () => ([
  {
    key: 'root',
    data: [],
  },
]);

export default atom({
  key: 'tables',
  default: getInitialTablesState(),
});
