import { atom } from 'recoil';
import { ICreateState } from './createState';

export interface IEditState extends Omit<ICreateState, 'age'> {
  agree: boolean;
  tableKey: string;
  index: number;
}

export const getInitialEditState: () => IEditState = () => ({
  name: '',
  surname: '',
  city: '',
  agree: false,
  tableKey: '',
  index: 0,
});

export default atom({
  key: 'editState',
  default: getInitialEditState(),
});
