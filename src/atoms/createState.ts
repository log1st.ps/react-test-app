import { atom } from 'recoil';

export interface ICreateState {
  name: string;
  surname: string;
  age: string;
  city: string;
}

export const getInitialCreateState: () => ICreateState = () => ({
  name: '',
  surname: '',
  age: '',
  city: '',
});

export default atom({
  key: 'createState',
  default: getInitialCreateState(),
});
