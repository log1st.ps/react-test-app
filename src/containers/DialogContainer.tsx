import * as React from 'react';
import { useEffect, useState } from 'react';
import { useRecoilValue } from 'recoil';
import ModalComponent from '../components/modal/ModalComponent';
import GridComponent from '../components/grid/GridComponent';
import ColumnComponent from '../components/grid/ColumnComponent';
import TextInputComponent from '../components/textInput/TextInputComponent';
import ActionsListComponent from '../components/actionsList/ActionsListComponent';
import CheckboxComponent from '../components/checkbox/CheckboxComponent';
import { IEditState } from '../atoms/editState';
import ErrorComponent from '../components/error/ErrorComponent';
import tablesState from '../atoms/tablesState';

const DialogContainer: React.FC<{
  value: IEditState,
  onInput?(value: IEditState): void;
  onSubmit?: (value: IEditState) => void;
  onClose?(): void;
}> = ({
  value,
  onInput,
  onSubmit,
  onClose,
}) => {
  const [isActive, setIsActive] = useState(false);
  const getHandler = (field: string) => (val: string) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    onInput && onInput({
      ...value,
      [field]: val,
    });
  };

  const close = () => {
    setIsActive(false);
    setTimeout(() => {
      // eslint-disable-next-line @typescript-eslint/no-unused-expressions
      onClose && onClose();
    }, 250);
  };

  useEffect(() => {
    requestAnimationFrame(() => {
      // eslint-disable-next-line @typescript-eslint/no-unused-expressions
      setIsActive && setIsActive(true);
    });
  }, [setIsActive]);

  const [validation, setValidation] = useState<{
    name?: string;
    surname?: string;
    city?: string;
    agree?: string;
  }>({});

  const tables = useRecoilValue(tablesState);

  const submitHandler = () => {
    const messages: typeof validation = {};
    if (!value.name.length) {
      messages.name = 'Name is empty';
    }
    if ((tables.find(
      (table) => table.key === value.tableKey,
    )?.data || []).findIndex((record, index) => (
      (index !== value.index) && record.name === value.name
    )) > -1) {
      messages.name = 'Name has been already taken';
    }
    if (!value.surname.length) {
      messages.surname = 'Surname is empty';
    }
    if (!value.agree) {
      messages.agree = 'Must agree';
    }
    if (!value.city.length) {
      messages.city = 'City is empty';
    }
    if (Object.keys(messages).length) {
      setValidation(messages);
      return;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    onSubmit && onSubmit(value);
    close();
  };

  return (
    <ModalComponent
      value={isActive}
      onInput={close}
      title="Edit name"
    >
      <GridComponent wrap>
        <ColumnComponent width="33.33333%">
          <TextInputComponent
            valid={!('name' in validation)}
            value={String(value.name)}
            onInput={getHandler('name')}
            placeholder="Name"
          />
          {'name' in validation && (
          <ErrorComponent text={validation.name} />
          )}
        </ColumnComponent>
        <ColumnComponent width="33.33333%">
          <TextInputComponent
            valid={!('surname' in validation)}
            value={String(value.surname)}
            onInput={getHandler('surname')}
            placeholder="Surname"
          />
          {'surname' in validation && (
          <ErrorComponent text={validation.surname} />
          )}
        </ColumnComponent>
        <ColumnComponent width="33.33333%">
          <TextInputComponent
            valid={!('city' in validation)}
            value={String(value.city)}
            onInput={getHandler('city')}
            placeholder="City"
          />
          {'city' in validation && (
          <ErrorComponent text={validation.city} />
          )}
        </ColumnComponent>
        <ColumnComponent width="33.33333%">
          <CheckboxComponent
            valid={!('agree' in validation)}
            value={value.agree}
            onInput={getHandler('agree')}
            label="Totally agree"
          />
          {'agree' in validation && (
          <ErrorComponent text={validation.agree} />
          )}
        </ColumnComponent>
        <ColumnComponent width="66.66667%">
          <ActionsListComponent
            actions={[
              {
                key: 'submit',
                label: 'SAVE',
                state: 'primary-filled',
                size: 'xl',
                onClick: submitHandler,
              },
            ]}
            grow
          />
        </ColumnComponent>
      </GridComponent>
    </ModalComponent>
  );
};

export default DialogContainer;
