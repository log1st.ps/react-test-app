import * as React from 'react';
import { useState } from 'react';
import { useRecoilValue } from 'recoil';
import TextInputComponent from '../components/textInput/TextInputComponent';
import GridComponent from '../components/grid/GridComponent';
import ColumnComponent from '../components/grid/ColumnComponent';
import ActionsListComponent from '../components/actionsList/ActionsListComponent';
import PanelComponent from '../components/panel/PanelComponent';
import { ICreateState } from '../atoms/createState';
import ErrorComponent from '../components/error/ErrorComponent';
import tablesState from '../atoms/tablesState';

const VerticalFormContainer: React.FC<{
  value: ICreateState,
  onInput?(value: ICreateState): void;
  onSubmit?: (value: ICreateState) => void;
}> = ({
  value,
  onInput,
  onSubmit,
}) => {
  const getHandler = (field: string) => (val: string) => {
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    onInput && onInput({
      ...value,
      [field]: val,
    });
  };

  const [validation, setValidation] = useState<{
    name?: string;
    surname?: string;
    city?: string;
    age?: string;
  }>({});

  const tables = useRecoilValue(tablesState);

  const submitHandler = () => {
    const messages: typeof validation = {};
    if (!value.name.length) {
      messages.name = 'Name is empty';
    }
    if ((tables[0].data || []).findIndex((record) => record.name === value.name) > -1) {
      messages.name = 'Name has been already taken';
    }
    if (!value.surname.length) {
      messages.surname = 'Surname is empty';
    }
    if (!value.age.length) {
      messages.age = 'Age is empty';
    }
    if (!value.city.length) {
      messages.city = 'City is empty';
    }
    setValidation(messages);
    if (Object.keys(messages).length) {
      return;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    onSubmit && onSubmit(value);
  };

  return (
    <PanelComponent>
      <GridComponent direction="column">
        <ColumnComponent>
          <TextInputComponent
            valid={!('name' in validation)}
            value={String(value.name)}
            onInput={getHandler('name')}
            placeholder="Name"
          />
          {'name' in validation && (
          <ErrorComponent text={validation.name} />
          )}
        </ColumnComponent>
        <ColumnComponent>
          <TextInputComponent
            valid={!('surname' in validation)}
            value={String(value.surname)}
            onInput={getHandler('surname')}
            placeholder="Surname"
          />
          {'surname' in validation && (
          <ErrorComponent text={validation.surname} />
          )}
        </ColumnComponent>
        <ColumnComponent>
          <TextInputComponent
            valid={!('age' in validation)}
            value={String(value.age)}
            onInput={getHandler('age')}
            placeholder="Age"
          />
          {'age' in validation && (
          <ErrorComponent text={validation.age} />
          )}
        </ColumnComponent>
        <ColumnComponent>
          <TextInputComponent
            valid={!('city' in validation)}
            value={String(value.city)}
            onInput={getHandler('city')}
            placeholder="City"
          />
          {'city' in validation && (
          <ErrorComponent text={validation.city} />
          )}
        </ColumnComponent>
        <ColumnComponent />
        <ColumnComponent>
          <ActionsListComponent
            actions={[
              {
                key: 'submit',
                label: 'ADD',
                state: 'primary-filled',
                size: 'xl',
                onClick: submitHandler,
              },
            ]}
            grow
          />
        </ColumnComponent>
      </GridComponent>
    </PanelComponent>
  );
};

export default VerticalFormContainer;
