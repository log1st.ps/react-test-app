const pxOrValue = (value?: string | number): string => (
  (typeof value === 'number' || (String(+(value || 0)) === value)) ? `${value}px` : (value || '')
);

export default pxOrValue;
